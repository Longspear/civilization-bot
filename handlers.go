package main

import (
tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
"database/sql"
_ "github.com/mattn/go-sqlite3"
"strconv"
"log"
"fmt"
"strings"
)

var activeUserID int64 = 0 // Global variable to track the active user


func handleChatIDCommand (bot *tgbotapi.BotAPI, message *tgbotapi.Message){
	chatid := "This chat ID is: " + strconv.FormatInt(message.Chat.ID, 10)
	msg := tgbotapi.NewMessage(message.Chat.ID, chatid)
	bot.Send(msg)
}

func handleMessage(bot *tgbotapi.BotAPI, message *tgbotapi.Message, db *sql.DB) error {

    userID := message.From.ID
    chatID := message.Chat.ID

    // Check if another user is currently active
    if activeUserID != 0 && activeUserID != userID {
        log.Printf("User is wrong")
        return nil
    }

    currentState := userStates[activeUserID]

    switch currentState {
        case READY:
            if message.IsCommand() {
                switch message.Command() {
                case "addkatka":
                    activeUserID = userID
                    selectedUsers = []string{}
                    timeatLA,_ := getCurrentTimeForLocation("America/Los Angeles")
                    gameName = timeatLA.Format("02.01.06")
                    userStates[userID] = WAITING_FOR_USERS
                    log.Printf("State set to WAITING_FOR_NAME for user %d", userID)
                    displayUsersAsInlineButtons(bot, chatID, db)
                    return nil                
                case "chatid":
                    handleChatIDCommand(bot, message)
                case "adduser":
                    args := message.CommandArguments()
                    if args != "" {
                        err := addUserToGroup(db, args)
                        if err != nil {
                            bot.Send(tgbotapi.NewMessage(chatID, "Failed to add user: "+err.Error()))
                        } else {
                            bot.Send(tgbotapi.NewMessage(chatID, "Пользователь добавлен!"))
                        }                        
                    }
                case "getkatka":
                    fetchGameInfo(bot, chatID, db, 0, 0)
                case "getusers":
                    fetchUserInfo(bot, chatID, db)
                case "searchkatka":
                    activeUserID = userID
                    userStates[userID] = WAITING_FOR_SEARCH_USERS
                    selectedUsers = []string{} // Resetting the selected users slice
                    displayUsersAsInlineButtons(bot, chatID, db)
                case "reset":
                        userStates[userID] = READY
                        bot.Send(tgbotapi.NewMessage(chatID, "Your state has been reset."))
//                case "mystate":
//                    currentStateString := botStateToString(userStates[userID])
//                    bot.Send(tgbotapi.NewMessage(chatID, "Your state is: "+currentStateString))   
                default:
                    // Check if it's a deletegame command (e.g., /remove1)
                    if message.Command() != "" {
                        command := message.Command()
                    
                        // Check if the command starts with "remove"
                        if strings.HasPrefix(command, "remove") {
                            removeCommand := "/" + command
                            err := deleteGameAndUserByRemoveCommand(db, removeCommand)
                            if err != nil {
                                bot.Send(tgbotapi.NewMessage(chatID, "Failed to delete game: "+err.Error()))
                            } else {
                                bot.Send(tgbotapi.NewMessage(chatID, "Удалено!"))
                            }
                        }
                    }
                }
            }
        case WAITING_FOR_NAME:
            if message.IsCommand() {
                switch message.Command() {
                case "reset":
                    userStates[userID] = READY
                    activeUserID = 0
                    bot.Send(tgbotapi.NewMessage(chatID, "Your state has been reset."))
                    return nil  // Add return here to exit the function
                case "mystate":
                    currentStateString := botStateToString(userStates[userID])
                    bot.Send(tgbotapi.NewMessage(chatID, "Your state is: "+currentStateString))
                    return nil  // Add return here to exit the function
                }
            } else if message.Text != "" && !message.IsCommand() {
                // This is where you handle plain text messages
                
                return nil  // Add return here to exit the function after processing the game name
            }
            
        case WAITING_FOR_SAVE_FILE_OWNER:
            // Handle the response when a user is selected as the save file owner
            if message.IsCommand() {
                switch message.Command() {
                case "reset":
                    userStates[userID] = READY
                    activeUserID = 0
                    bot.Send(tgbotapi.NewMessage(chatID, "Your state has been reset."))
                    return nil  // Add return here to exit the function
                case "mystate":
                    currentStateString := botStateToString(userStates[userID])
                    bot.Send(tgbotapi.NewMessage(chatID, "Your state is: "+currentStateString))
                    return nil  // Add return here to exit the function
                }
            } else {
                return nil  // Add return here to exit the function after processing the game name
            }

        case WAITING_FOR_USERS:
            if message.IsCommand() {
                switch message.Command() {
                case "reset":
                        userStates[userID] = READY
                        activeUserID = 0
                        bot.Send(tgbotapi.NewMessage(chatID, "Your state has been reset."))
                case "mystate":
                    currentStateString := botStateToString(userStates[userID])
                    bot.Send(tgbotapi.NewMessage(chatID, "Your state is: "+currentStateString))            
                }
            }

        default:
            activeUserID = 0
        }
    return nil
}

func botStateToString(state BotState) string {
    switch state {
    case READY:
        return "READY"
    case WAITING_FOR_NAME:
        return "WAITING_FOR_NAME"
    case WAITING_FOR_USERS:
        return "WAITING_FOR_USERS"
    default:
        return "UNKNOWN"
    }
}

func deleteGameAndUserByRemoveCommand(db *sql.DB, removeCommand string) error {
    // First, delete the game with the matching removeCommand from the 'games' table
    gameQuery := "DELETE FROM games WHERE removecommand = ?"
    gameStmt, err := db.Prepare(gameQuery)
    if err != nil {
        return err
    }
    defer gameStmt.Close()

    _, err = gameStmt.Exec(removeCommand)
    if err != nil {
        return err
    }

    // Then, delete the user with the matching userremovecommand from the 'groupusers' table
    userQuery := "DELETE FROM groupusers WHERE userremovecommand = ?"
    userStmt, err := db.Prepare(userQuery)
    if err != nil {
        return err
    }
    defer userStmt.Close()

    _, err = userStmt.Exec(removeCommand)
    if err != nil {
        return err
    }

    return nil
}

func handlePaginationCallback(bot *tgbotapi.BotAPI, callbackQuery *tgbotapi.CallbackQuery, db *sql.DB) error {
    callbackData := strings.Split(callbackQuery.Data, ":")
    if len(callbackData) != 2 {
        log.Printf("Invalid callback data: %s\n", callbackQuery.Data)
        return fmt.Errorf("invalid callback data")
    }

    newOffset, err := strconv.Atoi(callbackData[1])
    if err != nil {
        log.Printf("Error parsing offset: %s\n", err)
        return err
    }

    // Log the newOffset for debugging
    log.Printf("New Offset: %d\n", newOffset)

    // Call fetchGameInfo with the new offset and the original message's chat and message ID
    return fetchGameInfo(bot, callbackQuery.Message.Chat.ID, db, newOffset, callbackQuery.Message.MessageID)
}
