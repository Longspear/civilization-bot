package main

import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"log"
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
	"strings"
)

type GameInfo struct {
    GameID         int
    Name          string
    Users         string
    RemoveCommand string
	SaveOwner string
}

type UserInfo struct {
    UserID           int
    Username         string
    UserRemoveCommand string
}


type BotState int

const (
    READY BotState = iota
    WAITING_FOR_NAME
    WAITING_FOR_USERS
	WAITING_FOR_SAVE_FILE_OWNER
	WAITING_FOR_SEARCH_USERS
)

var userStates = make(map[int64]BotState)

var gameName string
var saveFileOwner string

func main() {

	token, err := readBotToken("./config/token.txt")
	if err != nil {
		log.Panicf("Token error: ", err)
	}
	bot, err := tgbotapi.NewBotAPI(token)
	if err != nil {
		log.Panic(err)
	}

	log.Printf("Authorized on account %s", bot.Self.UserName)
	
	db, err := sql.Open("sqlite3", "./mydb.db")
	if err != nil {
		log.Println(err)
		return
	}
	defer db.Close()

	_, err = db.Exec(`CREATE TABLE IF NOT EXISTS games (gameID INTEGER PRIMARY KEY, name TEXT, users TEXT, removecommand TEXT, saveowner TEXT)`)
	if err != nil {
		log.Println(err)
		return
	}

	_, err = db.Exec(`CREATE TABLE IF NOT EXISTS groupusers (userID INTEGER PRIMARY KEY, username TEXT, userremovecommand TEXT)`)
	if err != nil {
		log.Println(err)
		return
	}

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates := bot.GetUpdatesChan(u)

	for update := range updates {
		if update.Message != nil {
			m := update.Message
			if m.From == nil {
				continue
			}

			log.Printf("Message received")
			err := handleMessage(bot, m, db)
			if err != nil {
				log.Printf("[%s] %s,   err: %s", update.Message.From.UserName, update.Message.Text, err.Error())
				continue
			}

			log.Printf("[%s] %s", update.Message.From.UserName, update.Message.Text)
		}

		if update.CallbackQuery != nil {
			// This is where the inline button presses are handled
			userID := update.CallbackQuery.From.ID
			if strings.Contains(update.CallbackQuery.Data, "previous") || strings.Contains(update.CallbackQuery.Data, "next") {
				callbackConfig := tgbotapi.NewCallback(update.CallbackQuery.ID, "")
            	if _, err := bot.Request(callbackConfig); err != nil {
                	log.Printf("Error sending callback response: %s", err)
				}
				err := handlePaginationCallback(bot, update.CallbackQuery, db)
				if err != nil {
					log.Printf("Error handling pagination callback: %s", err)
				}
			} else
			if userStates[userID] == WAITING_FOR_USERS || userStates[userID] == WAITING_FOR_SEARCH_USERS {
				err := handleButtonPress(bot, update.CallbackQuery, db)
				if err != nil {
					log.Printf("Error handling button press: %s", err)
				}
			} else if userStates[userID] == WAITING_FOR_SAVE_FILE_OWNER {
				err := handleOwnerButtonPress(bot, update.CallbackQuery, db)
				if err != nil {
					log.Printf("Error handling button press: %s", err)
				}
			} else{
				callbackConfig := tgbotapi.NewCallback(update.CallbackQuery.ID, "Пошел нахуй")
            	if _, err := bot.Request(callbackConfig); err != nil {
                	log.Printf("Error sending callback response: %s", err)
				}
			}
		}
	}
}
