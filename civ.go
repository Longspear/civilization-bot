package main

import (
tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
"database/sql"
_ "github.com/mattn/go-sqlite3"
"fmt"
"strings"
"log"
"sort"
)

var selectedUsers []string

type User struct {
    Username   string
    IsSelected bool
}

var users []User

var sentMessageID int

func fetchGameInfo(bot *tgbotapi.BotAPI, chatID int64, db *sql.DB, offset int, messageID int) error {
    log.Printf("fetchGameInfo called with offset: %d, messageID: %d\n", offset, messageID)

    // Fetching game info from the database
    rows, err := db.Query("SELECT GameID, name, users, removecommand, saveowner FROM Games ORDER BY GameID LIMIT 5 OFFSET ?", offset)
    if err != nil {
        return err
    }
    defer rows.Close()

    var Games []GameInfo

    for rows.Next() {
        var mi GameInfo
        if err := rows.Scan(&mi.GameID, &mi.Name, &mi.Users, &mi.RemoveCommand, &mi.SaveOwner); err != nil {
            return err
        }
        Games = append(Games, mi)
    }

    // Check if no games were found
    if len(Games) == 0 {
        if offset > 0 {
            // Navigate back to the last available page
            return fetchGameInfo(bot, chatID, db, offset-5, messageID)
        }
        bot.Send(tgbotapi.NewMessage(chatID, "Нет нихуя"))
        return nil
    }

    var message string
    for _, m := range Games {
        message += fmt.Sprintf("№: %d - %s - Сейв овнер: %s - Игроки: %s - Удалить: %s\n", m.GameID, m.Name, m.SaveOwner, m.Users, m.RemoveCommand)
    }

    //Prepare inline keyboard for navigation
    var keyboardRows [][]tgbotapi.InlineKeyboardButton
    // Adding 'previous' button if not on the first page
    if offset > 0 {
        keyboardRows = append(keyboardRows, tgbotapi.NewInlineKeyboardRow(
            tgbotapi.NewInlineKeyboardButtonData("⬅️", fmt.Sprintf("previous:%d", offset-5))))
    }

    // Check if there are more entries for the 'next' page
    moreRows, err := db.Query("SELECT 1 FROM Games ORDER BY GameID LIMIT 1 OFFSET ?", offset+5)
    if err != nil {
        return err
    }
    hasMore := moreRows.Next()
    moreRows.Close()

    // Add 'next' button if more entries are available
    if hasMore {
        keyboardRows = append(keyboardRows, tgbotapi.NewInlineKeyboardRow(
            tgbotapi.NewInlineKeyboardButtonData("➡️", fmt.Sprintf("next:%d", offset+5))))
    }

    // Determine how to send the message based on whether the keyboard is empty
    if len(keyboardRows) == 0 {
        // Sending message without keyboard as there's no pagination needed
        msg := tgbotapi.NewMessage(chatID, message)
        _, err := bot.Send(msg)
        return err
    } else {
        // Sending message with keyboard for pagination
        keyboard := tgbotapi.NewInlineKeyboardMarkup(keyboardRows...)
        if messageID == 0 {
            // Sending a new message
            msg := tgbotapi.NewMessage(chatID, message)
            msg.ReplyMarkup = keyboard
            _, err := bot.Send(msg)
            return err
        } else {
            // Editing an existing message
            editMsg := tgbotapi.NewEditMessageText(chatID, messageID, message)
            editMsg.ReplyMarkup = &keyboard
            _, err := bot.Send(editMsg)
            return err
        }
    }
}

func askForGameName(bot *tgbotapi.BotAPI, chatID int64) (int, error) {
    msg := tgbotapi.NewMessage(chatID, "Дата и раунд игры?")
    sentMsg, err := bot.Send(msg)
    if err != nil {
        return 0, err // Return an error if the message couldn't be sent
    }
    return sentMsg.MessageID, nil // Return the MessageID of the sent message
}

func displayOwnersAsInlineButtons(bot *tgbotapi.BotAPI, chatID int64, messageID int, selectedUsers []string) error {
    // Create inline buttons for each selected user
    var buttons []tgbotapi.InlineKeyboardButton
    for _, user := range selectedUsers {
        button := tgbotapi.NewInlineKeyboardButtonData(user, user)
        buttons = append(buttons, button)
    }

    // Organize buttons into rows (e.g., 2 buttons per row)
    var keyboard [][]tgbotapi.InlineKeyboardButton
    for i := 0; i < len(buttons); i += 2 {
        row := make([]tgbotapi.InlineKeyboardButton, 0, 2)
        row = append(row, buttons[i])
        if i+1 < len(buttons) {
            row = append(row, buttons[i+1])
        }
        keyboard = append(keyboard, row)
    }

    // Send a message with these buttons
    markup := tgbotapi.NewInlineKeyboardMarkup(keyboard...)
    editMsg := tgbotapi.NewEditMessageText(chatID, messageID, "Выбери овнера:")
    editMsg.ReplyMarkup = &markup
    _, err := bot.Send(editMsg)
    if err != nil {
        return err
    }
    return nil
}

func displayUsersAsInlineButtons(bot *tgbotapi.BotAPI, chatID int64, db *sql.DB) error {
    // Fetch users from the database
    rows, err := db.Query("SELECT username FROM groupusers")
    if err != nil {
        return err
    }
    defer rows.Close()

    var users []string
    for rows.Next() {
        var username string
        if err := rows.Scan(&username); err != nil {
            return err
        }
        users = append(users, username)
    }

    // Check for error from iterating over rows
    if err = rows.Err(); err != nil {
        return err
    }

    // Create inline buttons for each user
    var buttons []tgbotapi.InlineKeyboardButton
    for _, user := range users {
        button := tgbotapi.NewInlineKeyboardButtonData(user, user)
        buttons = append(buttons, button)
    }

    // Organize buttons into rows (e.g., 2 buttons per row)
    var keyboard [][]tgbotapi.InlineKeyboardButton
    for i := 0; i < len(buttons); i += 2 {
        row := make([]tgbotapi.InlineKeyboardButton, 0, 2)
        row = append(row, buttons[i])
        if i+1 < len(buttons) {
            row = append(row, buttons[i+1])
        }
        keyboard = append(keyboard, row)
    }

    confirmButton := tgbotapi.NewInlineKeyboardButtonData("Подтвердить", "confirm_selection")
    keyboard = append(keyboard, []tgbotapi.InlineKeyboardButton{confirmButton})

    // Send a message with these buttons
    markup := tgbotapi.NewInlineKeyboardMarkup(keyboard...)
    editMsg := tgbotapi.NewMessage(chatID, "Выбери игроков:")
    editMsg.ReplyMarkup = &markup
    if err != nil {
        return err
    }
    sentMsg, err := bot.Send(editMsg)
    sentMessageID = sentMsg.MessageID
    return nil
}

func handleButtonPress(bot *tgbotapi.BotAPI, callbackQuery *tgbotapi.CallbackQuery, db *sql.DB) error {

    // Create the CallbackConfig
    callbackConfig := tgbotapi.NewCallback(callbackQuery.ID, "Ты нажал на кнопку")

    // Send the acknowledgment
    if _, err := bot.Request(callbackConfig); err != nil {
        log.Printf("Error answering callback query: %s", err)
        return err
    }
    
    userID := callbackQuery.From.ID
    chatID := callbackQuery.Message.Chat.ID
    data := callbackQuery.Data
    messageID := callbackQuery.Message.MessageID

    if data == "confirm_selection" && userStates[userID] == WAITING_FOR_USERS {
        // Insert game data into the database or perform other finalization steps
        // Example: insertGame(userID, selectedUsers, db)
        displayOwnersAsInlineButtons(bot, chatID, messageID, selectedUsers)
        userStates[userID] = WAITING_FOR_SAVE_FILE_OWNER
        return nil
    }

    if data == "confirm_selection" && userStates[userID] == WAITING_FOR_SEARCH_USERS {
        // Insert game data into the database or perform other finalization steps
        // Example: insertGame(userID, selectedUsers, db)
        searchGamesWithExactUsers(bot, chatID, messageID, selectedUsers, db)
        userStates[userID] = READY        
        return nil
    }

    // Extract user selection from callback data
    selectedUser := callbackQuery.Data

    // Toggle the user's selection and update the global state
    selectedUsers = toggleUserSelection(selectedUser)

    // Clear the existing users slice before fetching the updated state
    users = users[:0]

    // Fetch the updated list of users with their selection state
        var err error
    users, err = fetchUsersWithSelectionState(db)
    if err != nil {
        log.Printf("Error fetching updated users: %s", err)
        return err
    }

    // Rebuild the inline keyboard with the updated user selection states
    var buttons []tgbotapi.InlineKeyboardButton
    for _, user := range users {
        buttonText := user.Username
        if isSelected(user.Username) { // Check against selectedUsers slice
            buttonText += " ✅"
        }
        button := tgbotapi.NewInlineKeyboardButtonData(buttonText, user.Username)
        buttons = append(buttons, button)
    }

    // Organize buttons into rows as in displayUsersAsInlineButtons
    var keyboard [][]tgbotapi.InlineKeyboardButton
    for i := 0; i < len(buttons); i += 2 {
        row := make([]tgbotapi.InlineKeyboardButton, 0, 2)
        row = append(row, buttons[i])
        if i+1 < len(buttons) {
            row = append(row, buttons[i+1])
        }
        keyboard = append(keyboard, row)
    }
    confirmButton := tgbotapi.NewInlineKeyboardButtonData("Подтвердить", "confirm_selection")
    keyboard = append(keyboard, []tgbotapi.InlineKeyboardButton{confirmButton})

    // Edit the original message with the new inline keyboard
    markup := tgbotapi.NewInlineKeyboardMarkup(keyboard...)
    editMsg := tgbotapi.NewEditMessageReplyMarkup(chatID, callbackQuery.Message.MessageID, markup)
    bot.Send(editMsg)

    return nil
}

func insertGameIntoDatabase(db *sql.DB, gameName string, saveFileOwner string, selectedUsers []User) error {
    // Generate a unique gameID and removecommand
    var gameID int
    err := db.QueryRow("SELECT COALESCE(MAX(gameID), 0) FROM games").Scan(&gameID)
    if err != nil {
        return err
    }
    gameID++ // Increment the highest gameID to get a new unique ID

    removeCommand := fmt.Sprintf("/remove%d", gameID)

    // Extract usernames from the User structs
    var userNames []string
    for _, user := range selectedUsers {
        if user.IsSelected {
            userNames = append(userNames, user.Username)
        }
    }

    // Sort the usernames in alphabetical order
    sort.Strings(userNames)

    // Join the sorted usernames into a single string
    usersString := strings.Join(userNames, ", ")

    // Prepare the SQL statement for inserting the game
    stmt, err := db.Prepare("INSERT INTO games(gameID, name, users, removecommand, saveowner) VALUES(?, ?, ?, ?, ?)")
    if err != nil {
        return err
    }
    defer stmt.Close()

    // Execute the SQL statement
    _, err = stmt.Exec(gameID, gameName, usersString, removeCommand, saveFileOwner)
    return err
}


func addUserToGroup(db *sql.DB, username string) error {
    // 1. Нормализуем имя пользователя (убираем пробелы в начале и конце)
    username = strings.TrimSpace(username)
    if username == "" {
        return fmt.Errorf("Username cannot be empty")
    }

    // 2. Проверяем, не существует ли уже пользователь в таблице
    var existingUserID int
    err := db.QueryRow("SELECT userID FROM groupusers WHERE username = ?", username).Scan(&existingUserID)
    switch {
    case err == sql.ErrNoRows:
        // Пользователя нет, всё нормально — продолжаем
    case err != nil:
        // Произошла ошибка при запросе
        return err
    default:
        // err == nil: пользователь с таким именем уже есть
        return fmt.Errorf("user %s already exists in the DB", username)
    }

    // 3. Генерируем новый userID
    var userID int
    err = db.QueryRow("SELECT COALESCE(MAX(userID), 0) FROM groupusers").Scan(&userID)
    if err != nil {
        return err
    }
    userID++ // Инкрементируем, чтобы получить уникальный ID

    // 4. Создаем строку команды для удаления
    userRemoveCommand := fmt.Sprintf("/removeuser%d", userID)

    // 5. Готовим SQL-запрос для вставки
    stmt, err := db.Prepare("INSERT INTO groupusers (userID, username, userremovecommand) VALUES (?, ?, ?)")
    if err != nil {
        return err
    }
    defer stmt.Close()

    // 6. Выполняем запрос
    _, err = stmt.Exec(userID, username, userRemoveCommand)
    return err
}


func getSelectedUsers(db *sql.DB) ([]string, error) {
    // Prepare a slice to hold the usernames
    var users []string

    // Prepare the SQL query to fetch all usernames
    query := "SELECT username FROM groupusers"
    rows, err := db.Query(query)
    if err != nil {
        return nil, err
    }
    defer rows.Close()

    // Iterate through the rows and add usernames to the slice
    for rows.Next() {
        var username string
        if err := rows.Scan(&username); err != nil {
            return nil, err
        }
        users = append(users, username)
    }

    // Check for any error encountered during iteration
    if err = rows.Err(); err != nil {
        return nil, err
    }

    return users, nil
}

func toggleUserSelection(selectedUser string) []string {
    // Check if the user is already selected
    isSelected := false
    for i, user := range selectedUsers {
        if user == selectedUser {
            // User is already selected, remove them from the slice
            selectedUsers = append(selectedUsers[:i], selectedUsers[i+1:]...)
            isSelected = true
            break
        }
    }

    // If the user was not already selected, add them to the slice
    if !isSelected {
        selectedUsers = append(selectedUsers, selectedUser)
    }

    return selectedUsers
}



func fetchUsersWithSelectionState(db *sql.DB) ([]User, error) {

    // Fetch all users from the database
    rows, err := db.Query("SELECT username FROM groupusers")
    if err != nil {
        return nil, err
    }
    defer rows.Close()

    // Check each user if they are selected
    for rows.Next() {
        var username string
        if err := rows.Scan(&username); err != nil {
            return nil, err
        }

        user := User{
            Username:   username,
            IsSelected: isSelected(username),
        }
        users = append(users, user)
    }

    if err = rows.Err(); err != nil {
        return nil, err
    }

    return users, nil
}



// Helper function to check if a username is in the selected users slice
func isSelected(username string) bool {
    for _, user := range selectedUsers {
        if user == username {
            return true
        }
    }
    return false
}


func fetchUserInfo(bot *tgbotapi.BotAPI, chatID int64, db *sql.DB) error {
    // Query to select all users from the groupusers table
    rows, err := db.Query("SELECT userID, username, userremovecommand FROM groupusers")
    if err != nil {
        return err
    }
    defer rows.Close()

    var users []UserInfo

    // Iterate over the rows and scan data into UserInfo struct
    for rows.Next() {
        var ui UserInfo
        if err := rows.Scan(&ui.UserID, &ui.Username, &ui.UserRemoveCommand); err != nil {
            return err
        }
        users = append(users, ui)
    }

    // Check for any error encountered during iteration
    if err = rows.Err(); err != nil {
        return err
    }

    // Construct a message with user information
    var message string
    for _, u := range users {
        message += fmt.Sprintf("№: %d - %s - Удалить: %s\n", u.UserID, u.Username, u.UserRemoveCommand)
    }

    // Send the message to the chat
    msg := tgbotapi.NewMessage(chatID, message)
    bot.Send(msg)

    return nil
}

func handleOwnerButtonPress(bot *tgbotapi.BotAPI, callbackQuery *tgbotapi.CallbackQuery, db *sql.DB) error {

    // Create the CallbackConfig
    callbackConfig := tgbotapi.NewCallback(callbackQuery.ID, "Ты нажал на кнопку")

    // Send the acknowledgment
    if _, err := bot.Request(callbackConfig); err != nil {
        log.Printf("Error answering callback query: %s", err)
        return err
    }
    
    userID := callbackQuery.From.ID
    chatID := callbackQuery.Message.Chat.ID
    data := callbackQuery.Data
    messageID := callbackQuery.Message.MessageID

    saveFileOwner = data
    // Process the save file owner information
    // Transition to the next state or perform further actions
    insertGameIntoDatabase(db, gameName, saveFileOwner, users) // Use the global users slice
    // Transition back to READY state
    userStates[userID] = READY
    // Send confirmation message
    confirmationMsg := "Игра создана."
    bot.Send(tgbotapi.NewEditMessageText(chatID, messageID, confirmationMsg))
    activeUserID = 0

    return nil  // Add return here to exit the function after processing the game name
}

func searchGamesWithExactUsers(bot *tgbotapi.BotAPI, chatID int64, messageID int, selectedUsers []string, db *sql.DB) {
    // Sort the selectedUsers slice for consistent ordering
    sort.Strings(selectedUsers)

    // Join the selected usernames into a single string
    usersString := strings.Join(selectedUsers, ", ")

    // Query to find games with the exact list of users
    query := `SELECT GameID, name, users, saveowner 
              FROM games 
              WHERE users = ?`

    rows, err := db.Query(query, usersString)
    if err != nil {
        return
    }
    defer rows.Close()

    var Games []GameInfo

    for rows.Next() {
        var mi GameInfo
        if err := rows.Scan(&mi.GameID, &mi.Name, &mi.Users, &mi.SaveOwner); err != nil {
            return
        }
        Games = append(Games, mi)
    }

    // Check if no games were found
    if len(Games) == 0 {
        bot.Send(tgbotapi.NewEditMessageText(chatID, messageID, "Нет нихуя"))
        activeUserID = 0
        return
    }

    var message string
    for _, m := range Games {
        message += fmt.Sprintf("№: %d - %s - Сейв овнер: %s - Игроки: %s\n", m.GameID, m.Name, m.SaveOwner, m.Users)
    }
    msg := tgbotapi.NewEditMessageText(chatID, messageID, message)
    bot.Send(msg)
    activeUserID = 0
    return
}