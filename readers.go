package main

import (
//	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"bufio"
	"fmt"
	"os"
	"strings"
//	"io"
//	"net/http"
//	"path/filepath"
)

func readBotToken(filename string) (string, error) {
	file, err := os.Open(filename)
	if err != nil {
		return "", err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	if scanner.Scan() {
		return strings.TrimSpace(scanner.Text()), nil
	}

	return "", fmt.Errorf("no token found in %s", filename)
}

